var express = require('express');
var router = express.Router();
var request = require('request');
var requestingUrl = "http://127.0.0.1:3000";
var data = require('../data.json');
var authentication = ('./authentication.js');


/* GET home page. */
router.get('/', function(req, res, next) {
    // retrieves the cookie from the user.
    var auth_token = req.headers.cookie;
    var location = req.query.location;
    if(!location){location = "Durham"}
    // sends a get request for authentication with cookie and ip in header.
    request({
            url: requestingUrl+'/authentication',
            method:"GET",
            headers: {
                'cookie': auth_token,
                'ip': req.ip.split(":").pop()
            }},
        function (err, response, body) {
            console.log(body);
            var json = JSON.parse(body);
            // if auth_token and ip is accepted, render admin page
            if(json.accepted){
                request({
                        url: requestingUrl+'/authentication/username',
                        method:"GET",
                        headers: {
                            'cookie': auth_token
                        }},
                    function (err, response2, body2) {
                        console.log(body2);
                        var username = response2.body;
                        console.log(username);
                        res.render('schedule', {location: location});
                    })
            }

            //if rejected redirect to login page
            else(res.redirect('./login'))
        });
});

module.exports = router;

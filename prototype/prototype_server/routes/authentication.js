var express = require('express');
var router = express.Router();
var data = require('../data.json');
var sha256 = require('js-sha256');
var auth_tokens = {};

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

function getAuth(cookie) {
    var cookie_split = cookie.split("%3A");
    var cookie_time = cookie_split[0].split("=").pop();
    return auth_tokens[cookie_time].username;
}

router.get('/username',function (req, res, next) {
    res.setHeader('Content-Type', 'application/json');
    res.send(getAuth(req.headers.cookie))
});

/* GET authentication */
router.get('/', function(req, res, next) {
    res.setHeader('Content-Type', 'application/json');
    // retrieves the cookie and ip from the request header
    var cookie = req.headers.cookie;
    var ip = req.headers.ip;
    var accept = false;
    // regex to check if cookie is in the right format
    var pattern = /cookie=[0-9]*%3A[0-9]*/g;
    //The auth_token "concertina" should be valid for all times for IP addresses 129.239.xxx.yyy, set accept to true if so
    if (cookie === "concertina"){ //&& ip.substr(0, 7) === '129.234') {//===129.234.xxx.yyy
        accept = true;
    }
    //check there is a cookie and that it matches the regex.
    else if(cookie) {
        if(cookie.match(pattern)) {
            // split it up into the time the cookie was set and the random number
            var cookie_split = cookie.split("%3A");
            var cookie_time = cookie_split[0].split("=").pop();
            var cookie_check = cookie_split[1];
            //check if a cookie exists for that time and if so that the random number and ip also match. Set accept to true if this is so
            if (cookie_time) {
                if (auth_tokens[cookie_time]) {
                    //check that the cookie hasn't expired too
                    if (cookie_time + 7200000 > Date.now() && auth_tokens[cookie_time].ip === ip && auth_tokens[cookie_time].check === parseInt(cookie_check)) {
                        accept = true;
                    }
                }
            }
        }
    }
    //send the response which will either be true or false
    res.send({accepted: accept});
});

router.post('/', function(req, res, next){
    res.setHeader('Content-Type', 'application/json');
    //retrieve ip, username and password
    var ip = req.ip.split(":").pop();
    var username = req.body.username;
    var password = req.body.password + '1';
    console.log(username);
    console.log(password);
    //hash the password
    var hashedPassword = sha256(password);
    console.log(hashedPassword)
    //if username exists and the password matches...
    if(data.logins[username]){
        if(data.logins[username].pass===hashedPassword){
            //set the auth_time to Date.now()
            var auth_time = Date.now();
            //Generate a random 7 digit number
            var auth_random = getRandomInt(1000000,10000000);
            //concatenate them with a ':' this is the cookie sent to the user
            var auth_token = auth_time + ":" + auth_random;
            //add the auth_time as a key with the values check(the random 7 digit number),ip
            auth_tokens[auth_time]={"check":auth_random,"ip":ip,"username":username};
            //set the response cookie
            res.cookie("cookie",auth_token,{ maxAge: 7200000, httpOnly: true });
            //redirect to admin.html
            res.redirect('./profile');
        }
        else{
            //redirect to login with response of f to be false in order to inform of invalid credentials
            res.redirect('./login?res=f');
        }
    }
    else{
        res.redirect('./login?res=f');
    }
});

module.exports = router;

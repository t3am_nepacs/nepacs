/* Schedule Worker - Rhys */

function bookNow() {

    console.log("--- Booking Secured ---");

    /* For demonstration, forecefully change set columns 
    document.getElementById("m1").className = "col-2 time-slot time-slot-booked";
    document.getElementById("m2").className = "col-2 time-slot time-slot-booked"; */
    document.getElementById("m3").className = "col-2 time-slot time-slot-booked";

    /* Alert user */
    alert("Your slot has been booked. Please await confirmation from your supervisor.");
}

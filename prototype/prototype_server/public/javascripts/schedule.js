$(document).ready(function() {
	$(".time-slot").on("click", function() {
		$("#calendar-modal-outer").fadeIn("fast");
	});

	$("#calendar-modal .close").on("click", function() {
		$("#calendar-modal-outer").fadeOut("fast");
	});

	window.onclick = function(event) {
	    if (event.target === $("#calendar-modal-outer")[0]) {
	        $("#calendar-modal-outer").fadeOut("fast");
	    }
	}


	$(".time-slot").hover(function() {
		var className = this.classList[0];
		$("." + className).addClass("time-slot-hover");
	}, function() {
		var className = this.classList[0];
		$("." + className).removeClass("time-slot-hover");
	});
});

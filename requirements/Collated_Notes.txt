Charity details:

all prisons in the north east
136000 visits/year
120 people on average at any visit
struggle for volunteers
30 volunteers in durham
250 volunteers overall

Current solution:

They take bookings up to 1 month in advance
Employees can cancel at any point
Max 1 paid employee per site per shift
Current rota is an excel spreadsheet (separate one for each site)
Stores names of volunteers + contact + clearance level
The team leader at each site manages the rota
Team planner has to approve volunteers shifts because the charity pays for their expense
Concept IT run their current website so Alex needs to contact the CEO of the charity to get us access to the site to put a link on it to our system


Unanswered questions:
Is prison clearance for every prison or is it only valid for a specific prison?

When can we get all the contact details of the volunteers?

Can you adjust anyone's schedule at will?

Do people who handle the rotas currently at each site (managers) do shifts themselves? And are they paid? (Should they have separate accounts for admin and standard permissions?)

What are the various contracts offered to paid employees? (to attach a user's salary to their account to work out how much they are being paid with the hours that they have worked)

Can we have a copy of a recent rota to get more insight into how it is structured? (not to import)

Does the charity provide transport to shifts?

Clarify which roles staff can take on a shift. Does it vary by site?

Clarify: is the whole shift cancelled by the charity if there are less than 3 charity staff available?

Is there a minimum and maximum number of workers on a shift? And does there HAVE to be 1 paid worker on a shift or can it be just volunteers?

1 - system non-functional without this (MUST)
2 - system does what is required       (SHOULD)
3 - If there is time we can add this   (COULD)

Functional requirements:

Admin
  1- Admin can create new users
  1- Admin must confirm proposed shifts
  1- Admin confirms all new users
  1- Admin can change whether a standard user has prison clearance
  1- Only an admin can see which staff are paid on the calendar 
  1- Admin can manually create, modify and delete bookings (future and past)
  1- If paid worker books more hours than are in their contract, admin has to confirm
  
  2- Be able to create (puppet/ghost) user accounts for people who have limited contact methods (e.g. landline only)
  2- If a worker drops out at the last moment, admin can change the calendar to reflect who actually did the shift (give      admin account reminder)
  2- Admin can cancel a shift if it is undermanned, removing it from the history
  
  3- Admin can view a list of who has worked at a site before on calendar history
  3- Admin can view cancellation log and filter/search it by user and by site and date/time
  3- Option to log that a user arrive late or left early (paid and volunteers)
  
  
Standard user
  1- All users can see all shifts
  1- Can book shift at any site
  1- Must choose which role you are booking from
  1- Can cancel their own booking on the system
  
  2- Paid workers get priority booking
  
  3- Book recurring shift (same day and time), e.g. for paid workers with the same shifts each week/month
  3- Enter sites they are willing to work at/travel to (they can change this if they relocate)

System
  1- Able to handle up to 50 - 200 users concurrently
  1- Seperate calendar for each prison
  1- Prison clearance flag which restricts which shifts they can book - admin assigns to accounts after verifying externally from the system
  1- Restrict bookings to max 1 paid worker on a shift at each site (make this 1 person value changable)
  1- Responsive layout to work on all devices
  1- Store list of roles for each site
  1- Store min/max staff at each site on a shift
  1- Do not allow more bookings for a shift than the stored max staff for a shift at that site
  1- Shift time ranges (for each site) for calendar booking (e.g. there are no shifts before 9am)

  2- Secure sign up (admin sends account creation link or creates the account themselves for ghost users)
  2- Store modifiable list of roles for each site (admin can add/remove roles)
  2- Store modifiable min/max staff at each site on a shift (admin can change it)
  2- Modifiable shift time ranges (for each site) for calendar booking (e.g. there are no shifts before 9am)(admin can change)

  3- Notify the admin if an approaching shift is undermanned to give them chance to find people (direct them to the list of potentially available staff)
  3- Store how many hours each user thinks they will be able to work per week (maximum) to know who to contact to fill an undermanned shift if a person hasn't worked as many hours as they said they can
  3- Submit an optional text string with a booking that is info for the admin (e.g. I like Debby so if the shift is undermanned, contact Debby first)
  3- Provide list of people who might be available with their contact details to ask if they are available
  3- Have separate reserve list for people who will fill in for drop-outs from that shift
  3- Store which users cancel rather than removing it from the calendar and losing it (for analysis by admin)
  3- Store how long to wait before a shift starts if it is undermanned before notifying the admin that it is undermanned (so that they can start finding last-minute staff)
  3- colour coded calendar (paid + unpaid)
  3- Keep track of how many hours each user has worked (user can chose time frame) - only admin can view this
  3- Store contracted hours for paid workers


Non-functional requirements:

1 Admin per location multiple locations
1 Overall Admin (Alex)
Minimum/maximum shift time 2.5/11 hours (15 min blocks to make things easier?)
Easily navigatable/searchable calendar
Clean, modern UI



We will have a dummy application ready to show Alex face to face for demonstration before Dec 15th - book meeting

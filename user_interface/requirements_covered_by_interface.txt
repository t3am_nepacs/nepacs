Requirements Displayed in Designs

High Priority Implemented
	1. Staff can book at any site
	3. Staff will have prison clearance flags(customizable flags need to be implemented)
	4. Staff will have a first aid flag
	5. Staff will be able to see who else is working
	6. Staff can cancel bookings at any time
	7. Team leaders can create/modify/delete staff accounts
	8. Team leaders can update prison and first aid clearance flags
	9. Team leaders can approve or reject shifts at their site	
	11. System admin will be able to view everything and has same funcitonality as team leader
	12. System admin can create/modify/delete staff accounts
	14. Creation of new flags
	16. Validation, not relevant to UI design
	17. Multiple users, not relevenat to UI design
	18. Seperate booking calendar per site and paid worker highlighting

High Priority Not Implemented
	10. Team leaders have complete control of the timetable for their site
	13. Customizibility of prisons and roles
	15. Password recovery